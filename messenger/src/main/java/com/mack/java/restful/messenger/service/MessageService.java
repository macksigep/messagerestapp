package com.mack.java.restful.messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import com.mack.java.restful.messenger.database.DatabaseClass;
import com.mack.java.restful.messenger.exception.DataNotFoundException;
import com.mack.java.restful.messenger.model.Message;

public class MessageService {
	
	private Map<Long, Message> messages = DatabaseClass.getMessages();
	
	public MessageService(){ 
		 messages.put(1L, new Message(1, "My first REST Message", "Mack"));
		 messages.put(2L, new Message(2, "My second REST Message", "Mack")); 
	}

	public List<Message> getAllMessages(){
		return new ArrayList<Message>(messages.values());
	}
	
	public List<Message> getAllMessagesForYear(int year){
		List<Message> messagesForYear = new ArrayList<Message>();
		Calendar c = Calendar.getInstance(); 
		for(Message m: messages.values()){
			c.setTime(m.getCreated());
			if(c.get(Calendar.YEAR)==year)
				messagesForYear.add(m);
		} 
		return messagesForYear;
	}
	
	public List<Message> getAllMessagesPaginated(int start, int size){
		ArrayList<Message> paginatedList = new ArrayList<Message>(messages.values()); 
		if(start+size > paginatedList.size()) return new ArrayList<Message>();
		return  paginatedList.subList(start, start+size);
	}
	 
	
	public  Message  getMessage(long id){
		
		Message message = messages.get(id); 
		if(message==null){
			throw new DataNotFoundException("Message with id: " + id + " not found");
		} 
		return message;
		
	}
	
	public Message addMessage(Message m){ 
		m.setId(messages.size()+1);
		messages.put(m.getId(), m);
		return m;
	}
	
	public Message updateMessage(Message m){
		if(m.getId()<=0){return null;}
		messages.put(m.getId(), m);
		return m;
		
	}
	
	public Message removeMessage(long id){
		return messages.remove(id);
	}

}
