package com.mack.java.restful.messenger.database;

import java.util.HashMap;
import java.util.Map;

import com.mack.java.restful.messenger.model.Message;
import com.mack.java.restful.messenger.model.Profile;

public class DatabaseClass {
	
	private static Map<Long, Message> messages = new HashMap<>();
	private static Map<String, Profile> profiles = new HashMap<>();
			

	public DatabaseClass() {
	 
	}
	
	public static Map<Long, Message> getMessages(){ 
		return messages;
	}
	
	public static Map<String, Profile> getProfiles(){ 
		System.out.println(profiles.values().toString() + " profile out of Database class");
		return profiles;
	}

}
