package com.mack.java.restful.messenger.exception;

public class DataNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8515396414413078351L;

	public DataNotFoundException(String message) {
		super(message);
	}

}
