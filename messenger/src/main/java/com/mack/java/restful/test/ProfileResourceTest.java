package com.mack.java.restful.test;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mack.java.restful.messenger.model.Profile;
import com.mack.java.restful.messenger.service.ProfileService;


@Path("/profiles")
@Consumes(MediaType.APPLICATION_JSON) //<----annotates the whole class!
@Produces(MediaType.APPLICATION_JSON)  //<----annotates the whole class!

public class ProfileResourceTest {
	

	private ProfileService pService = new ProfileService();
	
	@GET
	public List<Profile> getProfiles( ){ 
		return pService.getAllProfiles();
	}
	
	
	
	
	
	

}
