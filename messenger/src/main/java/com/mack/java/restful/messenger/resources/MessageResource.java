package com.mack.java.restful.messenger.resources;

import java.util.List;

import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.mack.java.restful.messenger.model.Message;
import com.mack.java.restful.messenger.service.MessageService;

/*
 * Jersey looks for class with this classpath
 * 
 */
@Path("/messages") 
@Produces(MediaType.APPLICATION_JSON)  //<----annotates the whole class!
@Consumes(MediaType.APPLICATION_JSON) //<----annotates the whole class!
public class MessageResource {
	
	MessageService m = new MessageService();

	/*
	 * Once class page has been determined finds the @GET annotation and
	 * responds based on /messages directory
	 */
	@GET
	public List<Message> getMessages(@BeanParam MessageFilterBean filterBean){  
		if(filterBean.getYear()>0){return m.getAllMessagesForYear(filterBean.getYear());}
	    if(filterBean.getStart() >= 0 && filterBean.getSize() > 0 ){
	    	return m.getAllMessagesPaginated(filterBean.getStart(), filterBean.getSize());}
		return m.getAllMessages();
	}
	
	@POST 
	public Response addMessage(Message message){ 
		Message newMessage = m.addMessage(message);
		return Response.status(Status.CREATED)
				.entity(newMessage)
				.build(); 
	}
	
	/*
	 * Class directory path is appended with current directory
	 * will return message based on id /messages/1
	 * returns messages with that id
	 */
	@GET
	@Path("/{messageId}")
	public Message getMessage(@PathParam("messageId") long id){ 
		return m.getMessage(id); 
	}
	 
	
	@PUT
	@Path("/{messageId}")
	public Message updateMessage(@PathParam("messageId") long id, Message message){ 
		message.setId(id);
		return m.updateMessage(message);
	}
	
	@DELETE
	@Path("/{messageId}")
	public Message deleteMessage(@PathParam("messageId") long id){
		return m.removeMessage(id);
	}
	 
}
